import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-search-order',
  templateUrl: './search-order.component.html',
  styleUrls: ['./search-order.component.css']
})
export class SearchOrderComponent implements OnInit {
  found: boolean;
  showDetail: boolean;
  text: string;
  baseUrl = 'http://localhost:8081';
  searchInput: any;

  senderName: string;
  senderEmail: string;
  senderPhone: number;
  senderAddress: string;
  senderLocality: string;
  senderPostalCode: number;
  senderCountry: string;
  receiverName: string;
  receiverEmail: string;
  receiverPhone: number;
  receiverAddress: string;
  receiverLocality: string;
  receiverPostalCode: number;
  receiverCountry: string;
  packageLength: number;
  packageWidth: number;
  packageHeight: string;
  packageWeight: string;

  constructor(private router: Router, private http: HttpClient) {
  }

  ngOnInit() {
  }

  getShipment(form) {
    this.searchInput = form.value;
    const requestBody = {
      data: {
        ref: this.searchInput.ref_number
      }
    };
    const url = this.baseUrl + '/client/getshipment';
    this.http.post(url, requestBody).subscribe(
      (res: any) => {
        if (res.data.ref === '') {
          this.found = false;
          this.showDetail = false;
          this.text = 'Reference number not found';
        } else {
          this.showShipmentInfo(res.data);
          this.found = true;
          this.showDetail = true;
        }
      }
    );
  }

  hideInfo() {
    this.showDetail = false;
  }

  showShipmentInfo(result) {
    this.senderName = result.origin.contact.name;
    this.senderEmail = result.origin.contact.email;
    this.senderPhone = result.origin.contact.phone;
    this.senderAddress = result.origin.address.address_line1;
    this.senderLocality = result.origin.address.locality;
    this.senderPostalCode = result.origin.address.postal_code;
    this.senderCountry = result.origin.address.country_code;
    this.receiverName = result.destination.contact.name;
    this.receiverEmail = result.destination.contact.email;
    this.receiverPhone = result.destination.contact.phone;
    this.receiverAddress = result.destination.address.address_line1;
    this.receiverLocality = result.destination.address.locality;
    this.receiverPostalCode = result.destination.address.postal_code;
    this.receiverCountry = result.destination.address.country_code;
    this.packageLength = result.package.dimensions.length;
    this.packageWidth = result.package.dimensions.width;
    this.packageHeight = `${result.package.dimensions.height} ${result.package.dimensions.unit}`;
    this.packageWeight = `${result.package.grossWeight.amount} ${result.package.grossWeight.unit}`;
  }

  deleteThisShipment() {
    const deleteRequest = {
      data: {
         ref: this.searchInput.ref_number  // Use to lookup the shipment
      }
    };
    const url = this.baseUrl + '/client/deleteshipment';
    this.http.post(url, deleteRequest).subscribe(
      (res: any) => {
        console.log("delete succeed.");
      }
    );
  }

}

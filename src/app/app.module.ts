import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { FormComponentComponent } from './form-component/form-component.component';
import { SearchOrderComponent } from './search-order/search-order.component';

const appRoutes: Routes = [
  // {path: 'login', component: LoginComponent},
  // {path: 'shipment/list', component: ListComponent},
  {path: 'client/getquote', component: FormComponentComponent},
  // {path: 'shipment/detail/:ref', component: DetailComponent},
  // {path: '', component: HomeComponent},
  // {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    FormComponentComponent,
    SearchOrderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
